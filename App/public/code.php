<h1>Bind Paramaters to a Callable in PHP</h1>
<p>This could save a few keystrokes in some cases. It's certainly not important functionality, but it could be useful in some cases.</p>
<p>Create a class that takes a callable and list of paramaters into its constructor, then implement the <code>__invoke</code> magic method to make the new object callable as well. You can wrap the instantiation in a static function (<code>BoundClosure::bind($callable,$params)</code>) or in a global function (<code>function bindParams($callable,$params)</code>).</p>
<p>This class will do it. Just instantiate it (ex: <code>new BoundClosure('implode','-')</code>) or use the static (ex: <code>BoundClosure::bindParams('implode','-')</code>);</p>
<pre><code class="language-php">class BoundClosure {

    public function __construct($callable,...$params){
        $this-&gt;callable = $callable;
        $this-&gt;params = $params;
    }

    public function __invoke(...$args){
        $params = array_merge($this-&gt;params,$args);
        return call_user_func_array($this-&gt;callable,$params);
    }

    static function bindParams($callable,...$params){
        $closure = new BoundClosure($callable,...$params);
        return $closure;
    }
}
</code></pre>
<h2>Some improvements / tips</h2>
<ol>
<li>
<p>What if you want to leave the first paramater open &amp; bind to the second paramater? For example, maybe you want to get multiple substrings using the different haystacks, but the same start &amp; length. So you want to change <code>substr($haystack,4,6)</code> (to get the 4th-10th characters) to <code>$mySubstr($haystack)</code>. To do this, the closure-paramater binding function could accept <code>null</code>s &amp; on <code>__invoke</code> replace the <code>null</code>s with the passed in params, respectively. You could also use a numbered binding mechanism &amp; allow for chained binding, with something like: <code>$mySubstr = BoundClosure::bind('substr')-&gt;bind(4,1)-&gt;bind(6,2)</code> to bind the number <code>4</code> to the second paramater (0-based indexing) &amp; bind <code>6</code> to the third paramater.</p>
</li>
<li>
<p>I've used both a <code>public __construct</code>er &amp; a <code>static public bindParams(...)</code>... Having both of those isn't really necessary. Writing <code>new BoundClosure...</code> is a bit more clear &amp; concise than <code>BoundClosure::bindParams...</code>. Furthermore, the real keystroke saving version would utilize a global function. I stick, very strongly, to OOP, so I won't use a global function in my own code, but it could look something like this.</p>
</li>
</ol>
<pre><code class="language-php">function closureBind($callable,...$params){
    $obj = new Class(){
        public function __invoke(...$args){
            $params = array_merge($this-&gt;params,$args);
            return call_user_func_array($this-&gt;callable,$params);
        }
    };
    $obj-&gt;callable = $callable;
    $obj-&gt;params = $params;
    return $obj;
}
</code></pre>
<h2>The Test</h2>
<p>
I'm testing with the OOP class, NOT the global function &amp; I'll be using the bind to implode a 2-dimensional array without writing a function.</p>
<pre><code class="language-php">$data = [
    'animal' =&gt; [
        'dog','cat','lizard'
    ],
    'vehicle' =&gt; [
        'car','truck','boat',
    ],
];

//Since $data's elements are 1-dimensional arrays, mapping 'implode' will concatenate their values. 
//I couldn't normally map 'implode', because it requires the 'glue' to be passed as the first paramater
$implode = new BoundClosure('implode','-');
$data = array_map($implode,$data);
$str = implode('-',$data);
var_dump($str);
</code></pre>
