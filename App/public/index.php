<h1>Index Page</h1>
<p>This is a simple index page listing other kitchen-sink pages.</p>
<p>These kitchen sink pages are provided by <a href="https://tluf.me/taeluf">Taeluf's</a> <a href="https://tluf.me/php/lia/kitchen-sink">Kitchen Sink App</a>
<ul>
<li><a href="article/">Sample Article: Headings, paragraphs, dividers, image, timestamp, authorship</a></li>
<li><a href="article-with-section/">Sample Article: Same, but inside a &lt;section&gt;</a></li>
<li><a href="code/">Sample Code blocks &amp; stuff</a></li>
<li><a href="headers/">Headers: Just Headers</a></li>
<li><a href="images/">Images</a></li>
<li><a href="old-version/">The old Kitchen Sink page</a></li>
</ul>
