

<h1>Attributing an image</h1>
<h2>Without a wrapper</h2>
<img src="../right-to-repair.jpg" alt="Right To Repair photo by Jackie Filson / Open Markets Institute" />
<small>Thank you <a href="https://twitter.com/JackieFilson">Jackie Filson</a> and <a href="https://www.openmarketsinstitute.org/">Open Markets Institute</a> For this image.</small>
<small>The Attribution & This text are each inside a &lt;small&gt;</small>

<h2>In a &lt;section&gt;</h2>
<section>
<img src="../right-to-repair.jpg" alt="Right To Repair photo by Jackie Filson / Open Markets Institute" />
<small>Thank you <a href="https://twitter.com/JackieFilson">Jackie Filson</a> and <a href="https://www.openmarketsinstitute.org/">Open Markets Institute</a> For this image.</small>
</section>
<small>The Attribution & This text are each inside a &lt;small&gt;</small>
