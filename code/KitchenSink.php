<?php

namespace Lia\Addon;

class KitchenSink {


    protected $lia;

    static public function enable($lia){
        $instance = new static($lia);
    
        return $instance->init_liaison();
        // return $instance;
    }

    public function __construct($lia){
        $this->lia = $lia;
    }

    public function init_liaison(){
        $package = new \Lia\Package($this->lia, dirname(__DIR__,1).'/App/');
        return $package;
    }
}
